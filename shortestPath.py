#!flask/bin/python
from flask import Flask, jsonify
from math import radians, cos, sin, asin, sqrt
import json, time, sys
from neo4jrestclient.client import GraphDatabase
import numpy as np

def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    r = 6371 # Radius of earth in kilometers. Use 3956 for miles
    return c * r

if (len(sys.argv) != 2):
	print('Wrong number of arguments, expected 1 , got {}.'.format(len(sys.argv)-1))
	print('Usage : python shortestPath.py [PP: 0.2 || 0.025] optinal{database adress : default = http://localhost:7474}')
else:
	nodes = np.load('../importcsv-api/nodes.npy')
	PP = float(sys.argv[1])
	if(len(sys.argv) == 3):
		db = GraphDatabase(sys.argv[2])
	else:
		db = GraphDatabase("http://localhost:7474")
	xlimits = 90
	ylimits = -180
	app = Flask(__name__)
	
	def existingNode(id,x,y,cpt):
		if(nodes[int(x)][int(y)]):
			return id
		else:
			if(cpt % 8 == 0):
				tmpy = y - (cpt/8) + 1
				id = int(x*360/PP + tmpy)
				return existingNode(id,x,tmpy,cpt+1)
			if(cpt % 8 == 1):
				tmpy = y - (cpt/8) + 1
				tmpx = x - (cpt/8) + 1
				id = int(tmpx*360/PP + tmpy)
				return existingNode(id,tmpx,tmpy,cpt+1)
			if(cpt % 8 == 2):
				tmpx = x - (cpt/8) + 1
				id = int(tmpx*360/PP + y)
				return existingNode(id,tmpx,y,cpt+1)	
			if(cpt % 8 == 3):
				tmpy = y + (cpt/8) + 1
				tmpx = x - (cpt/8) + 1
				id = int(tmpx*360/PP + tmpy)
				return existingNode(id,tmpx,tmpy,cpt+1)	
			if(cpt % 8 == 4):
				tmpy = y + (cpt/8) + 1
				id = int(x*360/PP + y)
				return existingNode(id,x,tmpy,cpt+1)	
			if(cpt % 8 == 5):
				tmpy = y + (cpt/8) + 1
				tmpx = x + (cpt/8) + 1
				id = int(tmpx*360/PP + tmpy)
				return existingNode(id,tmpx,tmpy,cpt+1)
			if(cpt % 8 == 6):
				tmpx = x + (cpt/8) + 1
				id = int(tmpx*360/PP + y)
				return existingNode(id,tmpx,y,cpt+1)	
			if(cpt % 8 == 7):
				tmpy = y - (cpt/8) + 1
				tmpx = x + (cpt/8) + 1
				id = int(tmpx*360/PP + tmpy)
				return existingNode(id,tmpx,tmpy,cpt+1)	
	
	def getNodeId(long,lat,prec):
		long,lat = getNodeCoords(long,lat,prec)
		#Getting the nodeID that corresponds to long,lat present in db
		x = (xlimits - lat)/PP
		y = (long - ylimits)/PP
		id = int(x*360/PP + y)
		id = existingNode(id,x,y,0)
		print("using id => {}".format(id))
		return id

	def getNodeCoords(long,lat,prec):
		#Func to get closest coord present in db
		if (long%prec < 0.0005):
			ret_long = long
		else :
			ret_long = long+ (prec - (long%prec))
		if (lat%prec < 0.0005):
			ret_lat = lat
		else:
			ret_lat = lat - (lat%prec)
		return round(ret_long,6),round(ret_lat,6)
		
	def getPath(node1,node2):
		#Shortest Path Query
		query = "MATCH (start {nodeId:" + str(node1) + "}),(end  {nodeId:" + str(node2) + "}) "
		query += "CALL apoc.algo.aStar(start, end, 'ConnectedTo', 'cost', 'latitude', 'longitude') YIELD path AS path, weight AS weight RETURN path"
		print(query);
		results = db.query(query, data_contents=True).rows
		#Generating the path
		time = 0
		path = "["
		for i in range(0,len(results[0][0]),2):
			long1 = results[0][0][i]['longitude']
			lat1 = results[0][0][i]['latitude']
			try:
				long2 = results[0][0][i+2]['longitude']
				lat2 = results[0][0][i+2]['latitude']
				dist = haversine(long1,lat1,long2,lat2)
				try:
					time += dist  / results[0][0][i]['speed'] * 1.852
				except ZeroDivisionError:
					pass
				#print("time => {}".format(time))
			except IndexError:
				pass 
			except KeyError:
				pass
			path += "[" + str(long1) + "," + str(lat1) + "],"
		print(time)
		path = path[:-1]
		path += "]"
		return path,time

	def updateJsonFile(path,time):
		jsonFile = open("example.json", "r") # Open the JSON file for reading
		data = json.load(jsonFile) # Read the JSON into the buffer
		jsonFile.close() # Close the JSON file
		## Working with buffered content
		data["duration"] = json.loads(str(time))
		data["path"]["features"][0]["geometry"]["coordinates"] = json.loads(path)
		return jsonify(data)

	@app.route('/<string:latitude1>/<string:longitude1>/<string:latitude2>/<string:longitude2>', methods=['GET'])
	def get_path_string(latitude1,longitude1,latitude2,longitude2):
		node1 = getNodeId(float(longitude1),float(latitude1),PP)
		node2 = getNodeId(float(longitude2),float(latitude2),PP)
		path,time = getPath(node1,node2)
		response = updateJsonFile(path,time)
		response.headers.add('Access-Control-Allow-Origin', '*')
		return response

	if __name__ == '__main__':
		app.run(debug=True)
