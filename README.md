API to compute the shortest path algorithm between two points

Usage : python shortestPath.py
The API will be then exposed to : localhost:5000
Input : longitude1,latitude1,longitude2,latitude2
Output : json path

Example : localhost:5000/56.0/-20/45.5/21.0

Requirements : 

flask : pip install flask
neo4jrest-client : pip install neo4jrestclient

If you need pip, google is your friend ! 

APOC NEO4j : Running neo4j base with apoc plugin installed !
Check the importBase-api to load data into your neo4j db


In progress : 
	- Remove the example.json file 
	- Optimize node match on coordinates 
